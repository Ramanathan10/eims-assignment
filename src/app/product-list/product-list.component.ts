import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  productDetail: any;
  showCart: boolean = false;
  productData: any = [
    {
      "id": 1,
      "productName": "BLUETHOOTH MEDIA PLAYER",
      "productImage": "https://media.croma.com/image/upload/f_auto,q_auto,d_Croma%20Assets:no-product-image.jpg,h_170,w_170/h_300,w_300/v1605285895/Croma%20Assets/Entertainment/Speakers%20and%20Media%20Players/Images/8996897161246.png",
      "productPrice": "340",

    },
    {
      "id": 2,
      "productName": "ONEPLUS NORD",
      "productImage": "https://media.croma.com/image/upload/f_auto,q_auto,d_Croma%20Assets:no-product-image.jpg,h_170,w_170/h_300,w_300/v1605303556/Croma%20Assets/Communication/Mobiles/Images/8999479476254.png",
      "productPrice": "27,990",

    },
    {
      "id": 3,
      "productName": "REDMI NOTE 9",
      "productImage": "https://media.croma.com/image/upload/f_auto,q_auto,d_Croma%20Assets:no-product-image.jpg,h_170,w_170/h_300,w_300/v1605327400/Croma%20Assets/Communication/Mobiles/Images/8955975106590.png",
      "productPrice": "13,999",

    },
    {
      "id": 4,
      "productName": "APPLE IPHONE 12",
      "productImage": "https://media.croma.com/image/upload/f_auto,q_auto,d_Croma%20Assets:no-product-image.jpg,h_170,w_170/h_300,w_300/v1605327400/Croma%20Assets/Communication/Mobiles/Images/8955975106590.png",
      "productPrice": "84,900",

    },
    {
      "id": 5,
      "productName": "REALME 7",
      "productImage": "https://media.croma.com/image/upload/f_auto,q_auto,d_Croma%20Assets:no-product-image.jpg,h_170,w_170/v1606070899/Croma%20Assets/Communication/Mobiles/Images/8970870849566.png",
      "productPrice": "14,999",

    },
    {
      "id": 6,
      "productName": "VIVO V20 SE",
      "productImage": "https://media.croma.com/image/upload/f_auto,q_auto,d_Croma%20Assets:no-product-image.jpg,h_170,w_170/v1605341184/Croma%20Assets/Communication/Mobiles/Images/9002036920350.png",
      "productPrice": "20,990",

    }
  ]
  cartlist: any = [];
  constructor() {

  }

  ngOnInit(): void {
  }

  productCart(product, i) {
    const productExistInCart = this.cartlist.find(({ productName }) => productName === product.productName);
    if (!productExistInCart) {
      this.cartlist.push({ ...product, quantity: 1 });
      return;
    }
    productExistInCart.quantity += 1;
  }
  remove(product, i) {
    this.cartlist.splice(product)
    console.log('cart', this.cartlist)

  }
  plus(i) {
    this.cartlist[i].quantity += 1;
  }
  minus(i) {
    this.cartlist[i].quantity -= 1;

  }

}
